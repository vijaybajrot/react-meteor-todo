import { Meteor } from "meteor/meteor";
import { LinksCollection, TodosCollection } from "/imports/api/links";

function insertLink({ title, url }) {
  LinksCollection.insert({ title, url, createdAt: new Date() });
}

function seedDatabase() {
  insertLink({
    title: "Do the Tutorial",
    url: "https://www.meteor.com/tutorials/react/creating-an-app",
  });

  insertLink({
    title: "Follow the Guide",
    url: "http://guide.meteor.com",
  });

  insertLink({
    title: "Read the Docs",
    url: "https://docs.meteor.com",
  });

  insertLink({
    title: "Discussions",
    url: "https://forums.meteor.com",
  });

  TodosCollection.insert({
    title: "Todo1",
    completed: false,
    createdAt: new Date(),
  });
}

Meteor.startup(() => {
  // If the Links collection is empty, add some data.
  if (LinksCollection.find().count() === 0) {
    seedDatabase();
  }
});
