import { promisify } from "util";
import { Meteor } from "meteor/meteor";

export function callWithPromise(...args) {
  return promisify(Meteor.call).apply(Meteor, args);
}
