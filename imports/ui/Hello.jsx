import React, { useState, useEffect } from "react";
import { Meteor } from "meteor/meteor";
import { promisify } from "util";

import styles from "./style.scss";

function callWithPromise(...args) {
  return promisify(Meteor.call).apply(Meteor, args);
}

export const Hello = () => {
  const [counter, setCounter] = useState(0);

  const increment = () => {
    setCounter(counter + 1);
  };

  useEffect(() => {
    async function loadData() {
      const prom1 = await callWithPromise("links.list");
      console.log(prom1);
      const data = Meteor.call("links.list", (err, data) => {
        if (err) return err;
      });
    }
    loadData();
  }, []);

  return (
    <div className={styles.Hello}>
      <button onClick={increment}>Click Me</button>
      <p>You've pressed the button {counter} times.</p>
    </div>
  );
};
