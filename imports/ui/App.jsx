import React from "react";
import { Hello } from "./Hello.jsx";
// import { Info } from "./Info.jsx";
import { Todos } from "./Todo.jsx";
import StateFull from "./StateFull.jsx";
import theme from "/imports/ui/theme";
import { ThemeProvider, CSSReset, Text, Box } from "@chakra-ui/core";

export const App = () => (
  <ThemeProvider theme={theme}>
    <CSSReset />
    <Box maxWidth={"720px"} m={"auto"}>
      <Text fontSize="3xl" mb={"1em"}>
        React Meteor Todos App!
      </Text>
      <Todos />
    </Box>
  </ThemeProvider>
);
