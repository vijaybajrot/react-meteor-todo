import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";

export const Info = () => {
  const [items, setItems] = useState([]);
  const links = useTracker(() => {
    Meteor.call("links.list", (err, data) => {
      if (err) {
        return [];
      }
      setItems(data);
    });
  }, []);

  return (
    <div>
      <h2>Learn Meteor!</h2>
      <ul>
        {items.map((link) => (
          <li key={link._id}>
            <a href={link.url} target="_blank">
              {link.title}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};
