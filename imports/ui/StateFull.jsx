import React, { Component } from "react";
import { Button } from "@chakra-ui/core";

export default class StateFull extends Component {
  static IS_STATE = true;

  static async checkState() {
    console.log("isState", StateFull.IS_STATE);
  }

  state = {
    message: "This is state full component",
  };

  updateMessage() {
    this.setState({ message: "Message Updated" });
  }
  async componentDidMount() {
    await StateFull.checkState();
  }

  render() {
    const { message } = this.state;
    return (
      <div>
        {message}
        <Button onClick={this.updateMessage.bind(this)}>Update Message</Button>
      </div>
    );
  }
}
