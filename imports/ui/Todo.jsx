import React, { useState, useEffect } from "react";
import { Meteor } from "meteor/meteor";

import {
  FormControl,
  FormLabel,
  Button,
  Input,
  Box,
  Text,
  List,
  ListItem,
  ListIcon,
} from "@chakra-ui/core";

export const Todos = () => {
  const [completedCount, setCompletedCount] = useState(0);
  const [loading, setLoading] = useState(true);
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    Meteor.call("todos.list", (err, list) => {
      if (err) throw err;
      if (list) {
        setTodos(list);
        setLoading(false);
      }
    });

    Meteor.call("todos.completedCount", (err, count) => {
      if (err) return err;
      setCompletedCount(count);
    });
  }, []);

  const onTodoCreate = (todo) => {
    setTodos((prevTodos) => [todo, ...prevTodos]);
  };

  const markAsComplete = (todo) => {
    const completed = todo.completed === true ? false : true;
    Meteor.call("todos.markComplete", { id: todo._id, completed }, (err) => {
      if (!err) {
        setTodos((prevTodos) =>
          prevTodos.map((todoItem) => {
            if (todoItem._id === todo._id) {
              todoItem.completed = completed;
            }
            return todoItem;
          })
        );
        if (completed === true) {
          setCompletedCount((counts) => counts + 1);
        } else {
          setCompletedCount((counts) => counts - 1);
        }
      }
    });
  };

  if (loading) return <div>Loading...</div>;
  return (
    <div>
      <TodoForm onCreate={onTodoCreate} />
      <Text fontSize="md">Completed Todos : {completedCount}</Text>
      <Box mt={"1em"}>
        <List spacing={2}>
          {todos.map((todo) => {
            return (
              <ListItem
                key={todo._id}
                onClick={() => markAsComplete(todo)}
                textTransform="capitalize"
              >
                {todo.completed ? (
                  <ListIcon icon="check-circle" color="green.500" />
                ) : (
                  <ListIcon icon="circle" color="gray.500" />
                )}
                {todo.title}
              </ListItem>
            );
          })}
        </List>
      </Box>
    </div>
  );
};

export const TodoForm = ({ onCreate }) => {
  const [todo, setTodo] = useState("");

  const createTodo = (event) => {
    event.preventDefault();
    if (todo.trim().length < 1) {
      alert("Please Enter a Todo Title");
      return;
    }
    const data = { title: todo };
    Meteor.call("todos.create", data, (err, todo) => {
      if (err) console.log(err);
      setTodo("");
      onCreate(todo);
    });
  };

  return (
    <Box mb={"1em"}>
      <form onSubmit={createTodo}>
        <FormControl>
          <FormLabel htmlFor="todo">Todo </FormLabel>
          <Input
            type="text"
            id="todo"
            placeholder={"Todo Title"}
            value={todo}
            onChange={(e) => setTodo(e.target.value)}
          />
        </FormControl>

        <Button type={"submit"} mt={"0.5em"}>
          Create
        </Button>
      </form>
    </Box>
  );
};
