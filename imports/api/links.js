import { Mongo } from "meteor/mongo";
import { Meteor } from "meteor/meteor";
import { check } from "meteor/check";

export const LinksCollection = new Mongo.Collection("links");
export const TodosCollection = new Mongo.Collection("todos");

if (Meteor.isServer) {
  function todos() {
    return TodosCollection.find({}, { sort: { createdAt: -1 } }).fetch();
  }

  async function completedTodosCount() {
    const counts = await TodosCollection.rawCollection().countDocuments({
      completed: true,
    });

    return counts;
  }

  function createTodo({ title, completed = false }) {
    const todo = {
      title,
      completed: completed === true,
      createdAt: new Date(),
    };
    todo._id = TodosCollection.insert(todo);
    return todo;
  }

  function markTodoComplete({ id, completed }) {
    TodosCollection.update(id, { $set: { completed } });
    return true;
  }

  function links() {
    return LinksCollection.find({}).fetch();
  }

  Meteor.methods({
    "links.list": links,
    "todos.list": todos,
    "todos.create": createTodo,
    "todos.completedCount": completedTodosCount,
    "todos.markComplete": markTodoComplete,
  });
}
